#include "system.h"
#include <iostream>

#define MAX_BUFFER_SIZE 29521920

static Texture* clOutput = 0;
static Shader* shader = 0;

static Kernel* renderKernel;
static Kernel* render2Kernel;
static Kernel* resetKernel;
static Kernel* swapKernel;
static Kernel* simulateKernel;
static Kernel* clearKernel;

static Buffer* outputBuffer;
static Buffer* cells;
static Buffer* clsec;

// -----------------------------------------------------------
// Double-buffered bit pattern definition and access
// -----------------------------------------------------------
uint* pattern = 0, *second, pw = 0, ph = 0;
void BitSet(uint x, uint y) { pattern[y * pw + (x >> 5)] |= 1 << (x & 31); }
int GetBit(uint x, uint y) { return (second[y * pw + (x >> 5)] >> (x & 31)) & 1; }

// -----------------------------------------------------------
// Minimal loader for Golly .rle files
// -----------------------------------------------------------
void RLELoader(char* fileName)
{
	FILE* f = fopen(fileName, "r");
	if (!f) return;
	char buffer[1024], *p, c;
	int state = 0, n = 0, x = 0, y = 0;
	while (!feof(f))
	{
		buffer[0] = 0;
		fgets(buffer, 1023, f);
		if (*(p = buffer) == '#') continue; else if (*p == 'x')
		{
			if (sscanf(buffer, "x = %i, y = %i", &pw, &ph) != 2) return; // bad format
			pw = (pw + 31) / 32, pattern = (uint*)calloc(pw * ph, 4);
			continue;
		}
		while (1)
		{
			if ((c = *p++) < 32) break; // load next line
			if (state == 0) if (c < '0' || c > '9') state = 1, n = max(n, 1); else n = n * 10 + (c - '0');
			if (state == 1) // expect other character
			{
				if (c == '$') y += n, x = 0; // newline
				else if (c == 'o') for (int i = 0; i < n; i++) BitSet(x++, y); else if (c == 'b') x += n;
				state = n = 0;
			}
		}
	}
}

bool Game::Init()
{
	// load the pattern
	RLELoader("assets/turing_js_r.rle");				// 1,728 x 1,647
	//RLELoader( "assets/metapixel-galaxy.rle" );		// WARNING: 30,752 x 30,720, 113MB

	// enable for quadruple sized matrix (out of GPU memory testing)
	if (0)
	{
		uint* temp = (uint*)calloc(pw * ph * 4, 4);
		for (int i = 0; i < 4; i++)
		{
			memcpy(&temp[pw * ph * i], pattern, pw * ph * sizeof(uint));
		}
		pw *= 2; ph *= 2;
		pattern = (uint*)calloc(pw * ph, 4);
		memcpy(pattern, temp, pw * ph * sizeof(uint));
		free(temp);
	}

	second = (uint*)calloc(pw * ph, 4);

	// load shader and texture
	clOutput = new Texture( SCRWIDTH, SCRHEIGHT, Texture::FLOAT );
	shader = new Shader( "shaders/vignette.vert", "shaders/vignette.frag" );

	// load OpenCL code
	renderKernel = new Kernel("programs/program.cl", "render");
	render2Kernel = new Kernel("programs/program.cl", "render2");
	resetKernel = new Kernel("programs/program.cl", "reset");
	swapKernel = new Kernel("programs/program.cl", "swap");
	simulateKernel = new Kernel("programs/program.cl", "simulate");
	clearKernel = new Kernel("programs/program.cl", "clear");

	// link cl output texture as an OpenCL buffer
	outputBuffer = new Buffer( clOutput->GetID(), Buffer::TARGET );

	bufferSize = pw * ph;

	divNum = bufferSize / MAX_BUFFER_SIZE;
	divNum = divNum == 0 ? 1 : divNum;
	initSize = bufferSize / divNum;
	
	//&pattern[initSize * i]

	cells = new Buffer(initSize);
	uint* fc = (uint*)cells->GetHostPtr();
	memcpy(fc, pattern, initSize * sizeof(uint));
	cells->CopyToDevice();

	clsec = new Buffer(initSize);
	uint* fs = (uint*)clsec->GetHostPtr();
	memcpy(fs, second, initSize * sizeof(uint));
	clsec->CopyToDevice();
		
	renderKernel->SetArgument(0, outputBuffer);
	renderKernel->SetArgument(1, cells);
	renderKernel->SetArgument(2, (int)(pw / sqrtf(divNum)));
	renderKernel->SetArgument(3, (int)(ph / sqrtf(divNum)));
	renderKernel->SetArgument(4, offsetx);
	renderKernel->SetArgument(5, offsety);

	render2Kernel->SetArgument(0, outputBuffer);
	render2Kernel->SetArgument(1, cells);
	render2Kernel->SetArgument(2, (int)(pw / sqrtf(divNum)));
	render2Kernel->SetArgument(3, (int)(ph / sqrtf(divNum)));
	render2Kernel->SetArgument(4, offsetx);
	render2Kernel->SetArgument(5, offsety);

	resetKernel->SetArgument(0, cells);

	swapKernel->SetArgument(0, cells);
	swapKernel->SetArgument(1, clsec);

	simulateKernel->SetArgument(0, cells);
	simulateKernel->SetArgument(1, clsec);
	simulateKernel->SetArgument(2, (int)(pw / sqrtf(divNum)));
	simulateKernel->SetArgument(3, (int)(ph / sqrtf(divNum)));

	clearKernel->SetArgument(0, outputBuffer);

	// done
	return true;
}

// -----------------------------------------------------------
// Advance the pattern
// -----------------------------------------------------------
void Game::Simulate()
{
	if (divNum == 1)
	{
		swapKernel->Run(initSize);
		resetKernel->Run(initSize);
		simulateKernel->Run(initSize);
	}
	else
	{
		for (int i = 0; i < divNum; i++)
		{
			swapKernel->Run(initSize);
			resetKernel->Run(initSize);
			simulateKernel->Run(initSize);
			
			uint* fc = (uint*)cells->GetHostPtr();

			cells->CopyFromDevice();
			memcpy(&pattern[initSize * i], fc, initSize * sizeof(uint));
			memcpy(fc, &pattern[initSize * (i == (divNum - 1) ? 0 : (i + 1))], initSize * sizeof(uint));
			cells->CopyToDevice();

			uint* fs = (uint*)clsec->GetHostPtr();

			clsec->CopyFromDevice();
			memcpy(&second[initSize * i], fs, initSize * sizeof(uint));
			memcpy(fs, &second[initSize * (i == (divNum - 1) ? 0 : (i + 1))], initSize * sizeof(uint));
			clsec->CopyToDevice();
		}
	}
}

void Game::HandleInput()
{
	if (!GetAsyncKeyState(VK_LBUTTON)) mouseDown = false; else
	{
		POINT p;
		GetCursorPos(&p);
		if (mouseDown)
		{
			offsetx = offset0x + (dragx - p.x), offsety = offset0y + (dragy - p.y);
			offsetx = min((int)pw * 32 - SCRWIDTH, max(0, offsetx));
			offsety = min((int)ph - SCRHEIGHT, max(0, offsety));
			renderKernel->SetArgument(4, offsetx);
			renderKernel->SetArgument(5, offsety);
			return;
		}
		offset0x = offsetx, offset0y = offsety, dragx = p.x, dragy = p.y;
		mouseDown = true;
	}
}

void Game::Tick()
{
	Simulate();
	HandleInput();
	
	if (divNum == 1) renderKernel->Run(outputBuffer);
	else
	{
		for (int i = 0; i < divNum; i++)
		{
			int posX = i % (int)(sqrtf(divNum));
			int posY = i / (int)(sqrtf(divNum));

			render2Kernel->SetArgument(6, posX);
			render2Kernel->SetArgument(7, posY);

			uint* fc = (uint*)cells->GetHostPtr();
			memcpy(fc, &pattern[initSize * i], initSize * sizeof(uint));
			cells->CopyToDevice();

			render2Kernel->Run(outputBuffer);
		}
	}

	shader->Bind();
	shader->SetInputTexture( GL_TEXTURE0, "color", clOutput );
	shader->SetInputMatrix( "view", mat4::Identity() );

	DrawQuad();
}

void Game::Shutdown()
{
	delete clOutput;
	delete shader;
	delete renderKernel;
}

// EOF