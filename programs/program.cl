#include "../shared.h"
#include "randomnumbers.h"
#include "program.h"

__kernel void render(write_only image2d_t outimg, global uint* cells, unsigned int pw, unsigned int ph, int offsetx, int offsety)
{
	int x = get_global_id(0) + offsetx;
	int y = get_global_id(1) + offsety;
	if ((x >= SCRWIDTH + offsetx) || (y >= SCRHEIGHT + offsety)) return;
	if ((x < 0 + offsetx) || (y < 0 + offsety)) return;
	
	uint val = (cells[y * pw + (x >> 5)] >> (x & 31)) & 1;
	write_imagef(outimg, (int2)(x - offsetx, y - offsety), (float4)(val * 255, val * 255, val * 255, 1));
}

__kernel void render2(write_only image2d_t outimg, global uint* cells, unsigned int pw, unsigned int ph, int offsetx, int offsety, int posX, int posY)
{
	int x = get_global_id(0) + offsetx;
	int y = get_global_id(1) + offsety;
	if ((x >= SCRWIDTH + offsetx) || (y >= SCRHEIGHT + offsety)) return;
	if ((x < 0 + offsetx) || (y < 0 + offsety)) return;

	int startX = pw * 32 * posX;
	int startY = ph * posY;
	int limitX = pw * 32 * (posX + 1);
	int limitY = ph * (posY + 1);

	if ((x >= limitX) || (y >= limitY)) return;
	if ((x < startX) || (y < startY)) return;

	x -= startX;
	y -= startY;

	uint val = (cells[y * pw + (x >> 5)] >> (x & 31)) & 1;
	write_imagef(outimg, (int2)(x - offsetx, y - offsety), (float4)(val * 255, val * 255, val * 255, 1));
}

__kernel void reset(global uint* cells)
{
	int idx = get_global_id(0);
	cells[idx] = 0;
}

__kernel void swap(global uint* cells, global uint* clsec)
{
	int idx = get_global_id(0);
	uint tmp = cells[idx];
	cells[idx] = clsec[idx];
	clsec[idx] = tmp;
}

__kernel void simulate(global uint* cells, global uint* clsec, unsigned int pw, unsigned int ph)
{
	int idx = get_global_id(0);

	uint result = cells[idx];	

	for (int i = 0; i < 32; i++)
	{
		uint y = (idx / pw);
		uint x = ((idx % pw) * 32) + i;

		if (y == 0 || y == ph - 1) break;
		if (x == 0 || x == (pw * 32) - 1) continue;

		int posX, posY;
		int n = 0;

		for (int j = -1; j <= 1; j++) for (int k = -1; k <= 1; k++)
		{
			if (j == 0 && k == 0) continue;
			posY = y + j; posX = x + k;
			n += (clsec[posY * pw + (posX >> 5)] >> (posX & 31)) & 1;
		}

		int curr = (clsec[y * pw + (x >> 5)] >> (x & 31)) & 1;
		if ((curr && n == 2) || n == 3) result |= 1 << (x & 31);
	}

	cells[idx] = result;
}

__kernel void clear( write_only image2d_t outimg )
{
	uint x = get_global_id(0);
	uint y = get_global_id(1);
	const uint pixelIdx = x + y * SCRWIDTH;
	if (pixelIdx >= (SCRWIDTH * SCRHEIGHT)) return;
	write_imagef(outimg, (int2)(x, y), (float4)(0, 0, 0, 1));
}