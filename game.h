#pragma once

namespace ocllab {

class Game
{
public:
	bool Init();
	void Tick();
	void Shutdown();
	void HandleInput();
	void Simulate();
	void KeyDown( unsigned int key ) {}
	void KeyUp( unsigned int key ) {}
	void MouseMove( int x, int y ) {}
	void MouseClick() {}

private:
	int offsetx, offsety, offset0x, offset0y, dragx, dragy;
	bool mouseDown;
	uint bufferSize, divNum, initSize;
};
}

// EOF